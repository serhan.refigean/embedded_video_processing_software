#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std;
using namespace cv;

int main()
{
    VideoCapture input_video("../../input_videos/input_video_1.mp4");
    
    if(!input_video.isOpened())
    {
        cout << "Error: Could not open input video stream or file." << endl;
        return -1;
    }
    
    int frame_width = input_video.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = input_video.get(cv::CAP_PROP_FRAME_HEIGHT);
    
    double fps = input_video.get(cv::CAP_PROP_FPS);
    
    VideoWriter output_video("output_video.mp4", cv::VideoWriter::fourcc('m', 'p', '4', 'v'), fps, cv::Size(frame_width, frame_height), false);
    
    if(!output_video.isOpened())
    {
        cerr << "Error: Could not create output video file." << endl;
    }
    
    auto start = std::chrono::high_resolution_clock::now();
    
    while(1)
    {
        Mat input_frame;
        
        input_video >> input_frame;
        
        if(input_frame.empty())
            break;
        
        Mat output_frame(cv::Size(frame_width, frame_height), CV_8UC1);

        for(int i = 0; i < input_frame.rows; i++)
        {
            for(int j = 0; j < input_frame.cols; j++)
            {
                Vec3b rgb_pixel = input_frame.at<Vec3b>(i, j);
                int grayscale_pixel = 0.299 * rgb_pixel[2] + 0.587 * rgb_pixel[1] + 0.114 * rgb_pixel[0];
                output_frame.at<uchar>(i, j) = grayscale_pixel;
            }
        }
        
        output_video << output_frame;
    }
    
    auto end = std::chrono::high_resolution_clock::now();

    input_video.release();
    output_video.release();
    
    std::chrono::duration<double> elapsed = end - start;
    
    std::cout << "Elapsed time: " << elapsed.count() << " s" << endl;

    return 0;
}
