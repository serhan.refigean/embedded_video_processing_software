#include "color_to_grayscale.h"


void color_to_grayscale(hls::stream<rgb_pixel> &input_stream, hls::stream<grayscale_pixel> &output_stream)
{
    #pragma HLS INTERFACE axis port=input_stream
    #pragma HLS INTERFACE axis port=output_stream
    #pragma HLS INTERFACE s_axilite port=return

    rgb_pixel input_pixel;
    uint8 red_pixel, green_pixel, blue_pixel;
    uint8 grayscale;
    grayscale_pixel output_pixel;

    while (1)
    {
		input_stream.read(input_pixel);

		red_pixel = input_pixel.data.range(23,16);
		green_pixel = input_pixel.data.range(15,8);
		blue_pixel = input_pixel.data.range(7,0);

		grayscale = (red_pixel * 77 + green_pixel * 150 + blue_pixel * 29) >> 8;

		output_pixel.data = grayscale;

		output_pixel.keep = input_pixel.keep;
		output_pixel.strb = input_pixel.strb ;
		output_pixel.user = input_pixel.user;
		output_pixel.id = input_pixel.id;
		output_pixel.dest = input_pixel.dest;

	    if(input_pixel.last)
	    {
	    	output_pixel.last = 1;
	    	output_stream.write(output_pixel);
	        break;
	    }
	    else
	    {
	    	output_pixel.last = 0;
	    	output_stream.write(output_pixel);
	    }

    }
}
