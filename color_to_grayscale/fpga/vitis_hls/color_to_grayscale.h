#ifndef _COLOR_TO_GRAYSCALE_H_
#define _COLOR_TO_GRAYSCALE_H_

#include "ap_int.h"
#include "hls_stream.h"
#include "ap_axi_sdata.h"

typedef ap_uint<8> uint8;
typedef ap_axis<32, 2, 5, 6> rgb_pixel;
typedef ap_axis<8, 2, 5, 6> grayscale_pixel;

void color_to_grayscale(hls::stream<rgb_pixel> &input_stream, hls::stream<grayscale_pixel> &output_stream);

#endif
