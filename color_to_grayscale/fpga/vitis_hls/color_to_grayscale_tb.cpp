#include "color_to_grayscale.h"
#include <iostream>


int main() {
    hls::stream<rgb_pixel> input_stream;
    hls::stream<grayscale_pixel> output_stream;

    rgb_pixel pixel_in1;
    grayscale_pixel pixel_out;
    int num_errors = 0;

    for (int i = 0; i < 1920*1080; i++)
    {
		pixel_in1.data = 0x00123456;
		pixel_in1.keep = 1;
		pixel_in1.strb = 1;
		pixel_in1.user = 1;
		pixel_in1.id = 0;
		pixel_in1.dest = 1;

		if(i == 1920*1080 - 1)
		{
			pixel_in1.last = 1;

		}
		else
		{
			pixel_in1.last = 0;
		}

		input_stream.write(pixel_in1);
    }

    color_to_grayscale(input_stream, output_stream);


    for (int i = 0; i < 1920*1080; i++)
    {
        output_stream.read(pixel_out);

        uint8 expected_pixel = (0x12 * 77 + 0x34 * 150 + 0x56 * 29) >> 8;

        uint8 actual_pixel = pixel_out.data;

        if (expected_pixel != actual_pixel)
        {
            num_errors++;
        }
    }

    if (num_errors == 0) {
        std::cout << "Test Passed." << std::endl;
    } else {
        std::cout << "Test Failed with " << num_errors << " errors." << std::endl;
    }

    return 0;
}
