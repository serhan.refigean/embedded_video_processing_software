cmake_minimum_required(VERSION 3.22)
project(alpha_blending)
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
add_executable(alpha_blending alpha_blending.cpp)
target_compile_options(alpha_blending PRIVATE -O3 -ftree-vectorize)
target_link_libraries(alpha_blending ${OpenCV_LIBS})
