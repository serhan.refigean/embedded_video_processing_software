#ifndef _ALPHA_BLENDING_H_
#define _ALPHA_BLENDING_H_

#include "ap_int.h"
#include "hls_stream.h"
#include "ap_axi_sdata.h"

typedef ap_uint<8> uint8;
typedef ap_uint<16> uint16;
typedef ap_axis<32,2,5,6> rgb_pixel;

void alpha_blending(hls::stream<rgb_pixel> &input_stream_1, hls::stream<rgb_pixel> &input_stream_2, hls::stream<rgb_pixel> &output_stream);

#endif
