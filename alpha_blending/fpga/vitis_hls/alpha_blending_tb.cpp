#include "alpha_blending.h"
#include <iostream>


int main() {
    hls::stream<rgb_pixel> inputStream1, inputStream2, outputStream;

    rgb_pixel pixel_in1, pixel_in2, pixel_out;
    uint16 pixel_buffer;

    uint8 alpha_value = 13;

    int num_errors = 0;

    bool switch_streams = true;

	pixel_in1.data = alpha_value;
	pixel_in1.keep = 1;
	pixel_in1.strb = 1;
	pixel_in1.user = 1;
	pixel_in1.id = 0;
	pixel_in1.dest = 1;
	pixel_in1.last = 0;

	inputStream1.write(pixel_in1);


    for (int i = 0; i < 1920*1080; i++)
    {
		pixel_in1.data = 0x00123456;
		pixel_in1.keep = 1;
		pixel_in1.strb = 1;
		pixel_in1.user = 1;
		pixel_in1.id = 0;
		pixel_in1.dest = 1;

		pixel_in2.data = 0x00abcdef;
		pixel_in2.keep = 1;
		pixel_in2.strb = 1;
		pixel_in2.user = 1;
		pixel_in2.id = 0;
		pixel_in2.dest = 1;

		if(i == 1920*1080 - 1)
		{
			pixel_in1.last = 1;
			pixel_in2.last = 1;

		}
		else
		{
			pixel_in1.last = 0;
			pixel_in2.last = 0;
		}

		inputStream1.write(pixel_in1);
		inputStream2.write(pixel_in2);
    }


    alpha_blending(inputStream1, inputStream2, outputStream);


    for (int i = 0; i < 1920*1080; i++)
    {
        outputStream.read(pixel_out);


        pixel_buffer = alpha_value * 0x12 + (255 - alpha_value) * 0xab;
        uint8 expected_r = pixel_buffer / 255;

        pixel_buffer = alpha_value * 0x34 + (255 - alpha_value) * 0xcd;
        uint8 expected_g = pixel_buffer / 255;

        pixel_buffer = alpha_value * 0x56 + (255 - alpha_value) * 0xef;
        uint8 expected_b = pixel_buffer / 255;

        uint8 actual_r = pixel_out.data.range(23,16);
        uint8 actual_g= pixel_out.data.range(15,8);
        uint8 actual_b = pixel_out.data.range(7,0);

        if (actual_r != expected_r || actual_g != expected_g || actual_b != expected_b)
        {
            num_errors++;
        }
    }

    if (num_errors == 0) {
        std::cout << "Test Passed." << std::endl;
    } else {
        std::cout << "Test Failed with " << num_errors << " errors." << std::endl;
    }

    return 0;
}
