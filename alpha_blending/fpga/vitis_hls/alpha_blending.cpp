#include "alpha_blending.h"


void alpha_blending(hls::stream<rgb_pixel> &input_stream_1, hls::stream<rgb_pixel> &input_stream_2, hls::stream<rgb_pixel> &output_stream)
{
    #pragma HLS INTERFACE axis port=input_stream_1
    #pragma HLS INTERFACE axis port=input_stream_2
    #pragma HLS INTERFACE axis port=output_stream
    #pragma HLS INTERFACE s_axilite port=return

    rgb_pixel input_pixel_1, input_pixel_2, ouput_pixel;
    uint8 red_pixel_1, green_pixel_1, blue_pixel_1;
    uint8 red_pixel_2, green_pixel_2, blue_pixel_2;
    uint8 alpha_value;
    uint16 pixel_buffer;
    uint8 blended_red_pixel, blended_green_pixel, blended_blue_pixel;

	input_stream_1.read(input_pixel_1);
	alpha_value = input_pixel_1.data.range(7,0);

    while (1)
    {
		input_stream_1.read(input_pixel_1);
		red_pixel_1 = input_pixel_1.data.range(23,16);
		green_pixel_1 = input_pixel_1.data.range(15,8);
		blue_pixel_1 = input_pixel_1.data.range(7,0);

		input_stream_2.read(input_pixel_2);
		red_pixel_2 = input_pixel_2.data.range(23,16);
		green_pixel_2 = input_pixel_2.data.range(15,8);
		blue_pixel_2 = input_pixel_2.data.range(7,0);


	    pixel_buffer = alpha_value * red_pixel_1 + (255 - alpha_value) * red_pixel_2;
	    blended_red_pixel = pixel_buffer / 255;

	    pixel_buffer = alpha_value * green_pixel_1 + (255 - alpha_value) * green_pixel_2;
	    blended_green_pixel = pixel_buffer / 255;

	    pixel_buffer = alpha_value * blue_pixel_1 + (255 - alpha_value) * blue_pixel_2;
	    blended_blue_pixel = pixel_buffer / 255;



	    ouput_pixel.data.range(23,16) = blended_red_pixel;
	    ouput_pixel.data.range(15,8) = blended_green_pixel;
	    ouput_pixel.data.range(7,0) = blended_blue_pixel;

	    ouput_pixel.keep = input_pixel_1.keep;
	    ouput_pixel.strb = input_pixel_1.strb;
	    ouput_pixel.user = input_pixel_1.user;
	    ouput_pixel.id = input_pixel_1.id;
	    ouput_pixel.dest = input_pixel_1.dest;

	    if(input_pixel_1.last || input_pixel_2.last)
	    {
	    	ouput_pixel.last = 1;
	    	output_stream.write(ouput_pixel);
	        break;
	    }
	    else
	    {
	    	ouput_pixel.last = 0;
	    	output_stream.write(ouput_pixel);
	    }
    }
}
