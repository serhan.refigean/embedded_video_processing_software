#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>
#include <omp.h>

using namespace cv;
using namespace std;

int main()
{
    VideoCapture input_video_1("../../input_videos/input_video_1.mp4");
    
    if(!input_video_1.isOpened())
    {
        cout << "Error: Could not open the first input video stream or file." <<endl;
    }
    
    VideoCapture input_video_2("../../input_videos/input_video_2.mp4");
    
    if(!input_video_2.isOpened())
    {
        cout << "Error: Could not open the second input video stream or file." <<endl;
    }
    
    int frame_width_1 = input_video_1.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height_1 = input_video_1.get(cv::CAP_PROP_FRAME_HEIGHT);
    
    int frame_width_2 = input_video_2.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height_2 = input_video_2.get(cv::CAP_PROP_FRAME_HEIGHT);
    
    int same_size = 1;
    
    if (cv::Size(frame_width_1, frame_height_1) != cv::Size(frame_width_2, frame_height_2)) 
    {
        same_size = 0;
    }
    
    double fps = input_video_1.get(cv::CAP_PROP_FPS);
    
    VideoWriter output_video("output_video.mp4", cv::VideoWriter::fourcc('m', 'p', '4', 'v'), fps, cv::Size(frame_width_1, frame_height_1));
    
    if(!output_video.isOpened())
    {
        cerr << "Error: Could not create output video file." << endl;
    }
    
    auto start = std::chrono::high_resolution_clock::now();
    
    while(1)
    {
        Mat input_frame_1;
        
        input_video_1 >> input_frame_1;
        
        if(input_frame_1.empty())
            break;
            
        Mat input_frame_2;
        
        input_video_2 >> input_frame_2;
        
        if(input_frame_2.empty())
            break;
               
        if(same_size == 0)
        {
            cv::resize(input_frame_2, input_frame_2, input_frame_1.size());
        }
        
        double alpha_value = 0.5;

        Mat output_frame = Mat::zeros(cv::Size(frame_width_1, frame_height_1), input_frame_1.type());

        #pragma omp parallel for num_threads(6)
        for (int i = 0; i < frame_height_1; i++) 
        {
            for (int j = 0; j < frame_width_1; j++) 
            {
                for (int k = 0; k < input_frame_1.channels(); k++) 
                {
                    output_frame.at<Vec3b>(i, j)[k] = saturate_cast<uchar>(alpha_value * input_frame_1.at<Vec3b>(i, j)[k] + 
                                                                           (1 - alpha_value) * input_frame_2.at<Vec3b>(i, j)[k]);
                }
            }
        }

        output_video << output_frame;
    }
    
    auto end = std::chrono::high_resolution_clock::now();
    
    input_video_1.release();
    input_video_2.release();
    
    std::chrono::duration<double> elapsed = end - start;
    
    std::cout << "Elapsed time: " << elapsed.count() << " s" << endl;
   
    return 0;
}
