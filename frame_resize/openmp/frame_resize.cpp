#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>
#include <omp.h>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    VideoCapture input_video("../../input_videos/input_video_1.mp4");
    
    if(!input_video.isOpened())
    {
        cout << "Error: Could not open input video stream or file." << endl;
        return -1;
    }
    
    int frame_width = input_video.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = input_video.get(cv::CAP_PROP_FRAME_HEIGHT);
    
    double fps = input_video.get(cv::CAP_PROP_FPS);
    
    float scale_x = 2;

    float scale_y;
    
    if(argc == 2)
    {
        if(strcmp(argv[1], "4") == 0)
        {
            scale_y = 4;
        }
        else if(strcmp(argv[1], "8") == 0)
        {
            scale_y = 8;
        }
        else
        {
            scale_y = 2;
        }
    }
    else
    {
        scale_y = 2; 
    }

    int output_width = (float)frame_width / scale_x;
    int output_height = (float)frame_height / scale_y;
    
    VideoWriter output_video("output_video.mp4", cv::VideoWriter::fourcc('m', 'p', '4', 'v'), fps, cv::Size(output_width, output_height));
    
    if(!output_video.isOpened())
    {
        cerr << "Error: Could not create output video file." << endl;
    }

    auto start = std::chrono::high_resolution_clock::now();

    while(1)
    {
        Mat input_frame;
        
        input_video >> input_frame;
        
        if(input_frame.empty())
            break;
            
        Mat output_frame(output_height, output_width, input_frame.type());

        #pragma omp parallel for num_threads(6)
        for (int y = 0; y < output_height; y++)
        {
            for (int x = 0; x < output_width; x++)
            {
                float input_pixel_x = (float)(x * scale_x);
                float input_pixel_y = (float)(y * scale_y);

                int x0 = (int)floor(input_pixel_x);
                int y0 = (int)floor(input_pixel_y);
                int x1 = x0 + 1;
                int y1 = y0 + 1;

                float frac_x = input_pixel_x - x0;
                float frac_y = input_pixel_y - y0;

                x0 = max(0, min(x0, input_frame.cols - 1));
                x1 = max(0, min(x1, input_frame.cols - 1));
                y0 = max(0, min(y0, input_frame.rows - 1));
                y1 = max(0, min(y1, input_frame.rows - 1));

                Vec3b p00 = input_frame.at<Vec3b>(y0, x0);
                Vec3b p01 = input_frame.at<Vec3b>(y0, x1);
                Vec3b p10 = input_frame.at<Vec3b>(y1, x0);
                Vec3b p11 = input_frame.at<Vec3b>(y1, x1);

                Vec3b new_pixel = (1 - frac_x) * (1 - frac_y) * p00 + frac_x * (1 - frac_y) * p01 +
                                  (1 - frac_x) * frac_y * p10 + frac_x * frac_y * p11;

                output_frame.at<Vec3b>(y, x) = new_pixel;
            }
        }

        output_video << output_frame;
    }
    
    auto end = std::chrono::high_resolution_clock::now();

    input_video.release();
    output_video.release();
    
    std::chrono::duration<double> elapsed = end - start;
    
    std::cout << "Elapsed time: " << elapsed.count() << " s" << endl;

    return 0;
}
