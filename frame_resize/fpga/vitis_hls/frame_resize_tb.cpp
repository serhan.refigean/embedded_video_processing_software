#include "frame_resize.h"
#include <iostream>
#include "hls_stream.h"
#include <cstdlib>
#include <ctime>


void init_test_stream(hls::stream<rgb_pixel> &stream, uint32 width, uint32 height, uint32 x, uint32 y)
{
    rgb_pixel pixel;

    uint32 arguments[4] = {width, height, x, y};

    for(int i = 0; i < 4; i++)
    {
		pixel.data = arguments[i];
		pixel.keep = 1;
		pixel.strb = 1;
		pixel.user = 1;
		pixel.id = 0;
		pixel.dest = 1;
		pixel.last = 0;

		stream.write(pixel);
    }

    srand(time(0));

    for(int i = 0; i < width * height; i++)
    {
        unsigned int random_pixel = rand() % 256;
        pixel.data = (random_pixel << 16) | (random_pixel << 8) | random_pixel;

        pixel.keep = 1;
        pixel.strb = 1;
        pixel.user = 1;
        pixel.id = 0;
        pixel.dest = 1;

        pixel.last = (i == (width*height - 1)) ? 1 : 0;

        stream.write(pixel);
    }
}

int main()
{
    hls::stream<rgb_pixel> input_stream;
    hls::stream<rgb_pixel> output_stream;

    uint32 original_width = 1920;
    uint32 original_height = 1080;
    uint32 scale_x = 2;
    uint32 scale_y = 2;

    init_test_stream(input_stream, original_width, original_height, scale_x, scale_y);

    frame_resize(input_stream, output_stream);


    int output_size = 0;

    while (!output_stream.empty())
    {
        rgb_pixel pixel = output_stream.read();
        output_size++;
    }

    int expected_size = (original_width / scale_x) * (original_height / scale_y);

    if(output_size == expected_size)
    {
        std::cout << "Test Passed." << std::endl;
    }
    else
    {
        std::cout << "Test Failed." << std::endl;
    }

    return 0;
}
