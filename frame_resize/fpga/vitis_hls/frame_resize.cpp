#include "frame_resize.h"
#include "math.h"

#define MAX_WIDTH 1920
#define MAX_HEIGHT 1080

inline int max(int a, int b)
{
    return (a > b) ? a : b;
}

inline int min(int a, int b)
{
    return (a < b) ? a : b;
}

void frame_resize(hls::stream<rgb_pixel> &input_stream, hls::stream<rgb_pixel> &output_stream)
{
    #pragma HLS INTERFACE axis port=input_stream
    #pragma HLS INTERFACE axis port=output_stream
    #pragma HLS INTERFACE s_axilite port=return

    rgb_pixel input_pixel;
    rgb_pixel output_pixel;

    input_stream.read(input_pixel);
    uint32 width = input_pixel.data;

    input_stream.read(input_pixel);
    uint32 height = input_pixel.data;

    input_stream.read(input_pixel);
	uint32 scale_x = input_pixel.data;

	input_stream.read(input_pixel);
	uint32 scale_y = input_pixel.data;

    int output_width = (float)width / scale_x;
    int output_height = (float)height / scale_y;

    int i, j;

    three_channel_pixel input_image[2][MAX_WIDTH];
    int last_y = -1;


    for(int y = 0; y < output_height; y++)
    {
        float input_pixel_y = (float)(y * scale_y);
        int y0 = (int)floor(input_pixel_y);
        int y1 = y0 + 1;

        if(y0 != last_y)
        {
            for(int x = 0; x < width; x++)
            {
                input_stream.read(input_pixel);
                input_image[0][x] = input_pixel.data;
            }
            for(int x = 0; x < width; x++)
            {
                input_stream.read(input_pixel);
                input_image[1][x] = input_pixel.data;
            }

            last_y = y0;
        }

    	for(int x = 0; x < output_width; x++)
    	{
            float input_pixel_x = (float)(x * scale_x);
            int x0 = (int)floor(input_pixel_x);
            int x1 = x0 + 1;

            float frac_x = input_pixel_x - x0;
            float frac_y = input_pixel_y - y0;

            x0 = max(0, min(x0, width - 1));
            x1 = max(0, min(x1, width - 1));
            y0 = max(0, min(y0, height - 1));
            y1 = max(0, min(y1, height - 1));

            uint8 p00_r = input_image[0][x0].range(23, 16);
            uint8 p00_g = input_image[0][x0].range(15, 8);
            uint8 p00_b = input_image[0][x0].range(7, 0);

            uint8 p01_r = input_image[0][x1].range(23, 16);
            uint8 p01_g = input_image[0][x1].range(15, 8);
            uint8 p01_b = input_image[0][x1].range(7, 0);

            uint8 p10_r = input_image[1][x0].range(23, 16);
            uint8 p10_g = input_image[1][x0].range(15, 8);
            uint8 p10_b = input_image[1][x0].range(7, 0);

            uint8 p11_r = input_image[1][x1].range(23, 16);
            uint8 p11_g = input_image[1][x1].range(15, 8);
            uint8 p11_b = input_image[1][x1].range(7, 0);

            uint8 output_r = (1 - frac_x) * (1 - frac_y) * p00_r + frac_x * (1 - frac_y) * p01_r +
                    (1 - frac_x) * frac_y * p10_r + frac_x * frac_y * p11_r;

            uint8 output_g = (1 - frac_x) * (1 - frac_y) * p00_g + frac_x * (1 - frac_y) * p01_g +
                    (1 - frac_x) * frac_y * p10_g + frac_x * frac_y * p11_g;

            uint8 output_b = (1 - frac_x) * (1 - frac_y) * p00_b + frac_x * (1 - frac_y) * p01_b +
                    (1 - frac_x) * frac_y * p10_b + frac_x * frac_y * p11_b;

            output_pixel.data.range(23,16) = output_r;
            output_pixel.data.range(15,8) = output_g;
            output_pixel.data.range(7,0) = output_b;

    		output_pixel.keep = input_pixel.keep;
    		output_pixel.strb = input_pixel.strb;
    		output_pixel.user = input_pixel.user;
    		output_pixel.id = input_pixel.id;
    		output_pixel.dest = input_pixel.dest;

    	    if(y == output_height - 1 && x == output_width - 1)
    	    {
    	    	output_pixel.last = 1;
    	    }
    	    else
    	    {
    	    	output_pixel.last = 0;
    	    }

    	    output_stream.write(output_pixel);
    	}
    }
}
