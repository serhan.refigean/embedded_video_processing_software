#ifndef _FRAME_CROPPING_H_
#define _FRAME_CROPPING_H_

#include "ap_int.h"
#include "hls_stream.h"
#include "ap_axi_sdata.h"

typedef ap_uint<8> uint8;
typedef ap_uint<32> uint32;
typedef ap_axis<32,2,5,6> rgb_pixel;

void frame_cropping(hls::stream<rgb_pixel> &input_stream, hls::stream<rgb_pixel> &output_stream);

#endif
