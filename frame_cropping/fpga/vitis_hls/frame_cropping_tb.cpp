#include "frame_cropping.h"
#include <iostream>


int main()
{
    hls::stream<rgb_pixel> input_stream;
    hls::stream<rgb_pixel> output_stream;

    rgb_pixel input_pixel;
    rgb_pixel output_pixel;

    int error_counter = 0;
    int output_size = 0;

    uint32 arguments[6];

    // Width of the input frame
    arguments[0] = (uint32) 1920;

    // Height of the input frame
    arguments[1] = (uint32) 1080;

    // Coordinate on the x axis for the top left corner of the cropped image
	arguments[2] = (uint32) 50;

	// Coordinate on the y axis for the top left corner of the cropped image
	arguments[3] = (uint32) 60;

	// Width of the cropped frame
	arguments[4] = (uint32) 900;

	// Height of the cropped frame
	arguments[5] = (uint32) 900;

    for(int i = 0; i < 6; i++)
    {
		input_pixel.data = arguments[i];
		input_pixel.keep = 1;
		input_pixel.strb = 1;
		input_pixel.user = 1;
		input_pixel.id = 0;
		input_pixel.dest = 1;
		input_pixel.last = 0;

		input_stream.write(input_pixel);
    }

    for(int i = 0; i < 1920 * 1080; i++)
    {
		input_pixel.data = 0x00123456;
		input_pixel.keep = 1;
		input_pixel.strb = 1;
		input_pixel.user = 1;
		input_pixel.id = 0;
		input_pixel.dest = 1;

		if(i == 1920 * 1080 - 1)
		{
			input_pixel.last = 1;

		}
		else
		{
			input_pixel.last = 0;
		}

		input_stream.write(input_pixel);
    }

    frame_cropping(input_stream, output_stream);

	while(1)
	{
		output_stream.read(output_pixel);
		output_size++;

		if(output_pixel.data != 0x00123456)
		{
			error_counter++;
		}

		if(output_pixel.last == 1)
		{
			break;
		}

    }

    if(error_counter == 0 && output_size == 810000)
    {
        std::cout << "Test Passed." << std::endl;
    }
    else
    {
        std::cout << "Test Failed with " << error_counter << " errors." << std::endl;
    }

    return 0;
}
