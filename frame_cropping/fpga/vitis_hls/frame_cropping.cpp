#include "frame_cropping.h"


void frame_cropping(hls::stream<rgb_pixel> &input_stream, hls::stream<rgb_pixel> &output_stream)
{
    #pragma HLS INTERFACE axis port=input_stream
    #pragma HLS INTERFACE axis port=output_stream
    #pragma HLS INTERFACE s_axilite port=return

    rgb_pixel input_pixel;
    rgb_pixel output_pixel;

    input_stream.read(input_pixel);
    uint32 width = input_pixel.data;

    input_stream.read(input_pixel);
    uint32 height = input_pixel.data;

	input_stream.read(input_pixel);
	uint32 x = input_pixel.data;

	input_stream.read(input_pixel);
	uint32 y = input_pixel.data;

	input_stream.read(input_pixel);
	uint32 cropped_width = input_pixel.data;

	input_stream.read(input_pixel);
	uint32 cropped_height = input_pixel.data;

    // Index for image height
    int i = 0;

    // Index for image width
    int j = -1;

    while (1)
    {
    	j++;

    	input_stream.read(input_pixel);

    	if(i >= y && i < y + cropped_height && j >= x && j < x + cropped_width)
    	{
			output_pixel.data = input_pixel.data;
			output_pixel.keep = input_pixel.keep;
			output_pixel.strb = input_pixel.strb;
			output_pixel.user = input_pixel.user;
			output_pixel.id = input_pixel.id;
			output_pixel.dest = input_pixel.dest;

    		if(i == y + cropped_height - 1 && j == x + cropped_width - 1)
    		{
    		    while(input_pixel.last != 1)
    		    {
    		    	input_stream.read(input_pixel);
    		    }

    			output_pixel.last = 1;
    			output_stream.write(output_pixel);
    			return;
    		}

    		output_pixel.last = 0;
    		output_stream.write(output_pixel);
    	}

		if(j == width - 1)
		{
			j = -1;
			i++;
		}
    }
}
