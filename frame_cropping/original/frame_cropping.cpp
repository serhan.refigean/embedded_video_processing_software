#include <iostream>
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std;
using namespace cv;

int main()
{
    VideoCapture input_video("../../input_videos/input_video_1.mp4");
    
    if(!input_video.isOpened())
    {
        cout << "Error: Could not open input video stream or file." << endl;
        return -1;
    }
    
    int frame_width = input_video.get(cv::CAP_PROP_FRAME_WIDTH);
    int frame_height = input_video.get(cv::CAP_PROP_FRAME_HEIGHT);
    
    double fps = input_video.get(cv::CAP_PROP_FPS);
    
    int x = 50;
    int y = 60;

    int width = 900;
    int height = 900;
    
    VideoWriter output_video("output_video.mp4", cv::VideoWriter::fourcc('m', 'p', '4', 'v'), fps, cv::Size(width, height));
    
    if(!output_video.isOpened())
    {
        cerr << "Error: Could not create output video file." << endl;
    }

    auto start = std::chrono::high_resolution_clock::now();

    while(1)
    {
        Mat input_frame;
        
        input_video >> input_frame;
        
        if(input_frame.empty())
            break;
        
        Mat output_frame(cv::Size(width, height), input_frame.type());
        
        for (int i = y; i < y + height; i++) 
        {
            for (int j = x; j < x + width; j++) 
            {
                output_frame.at<Vec3b>(i - y, j - x) = input_frame.at<Vec3b>(i, j);
            }
        }

        output_video << output_frame;
    }
    
    auto end = std::chrono::high_resolution_clock::now();

    input_video.release();
    output_video.release();
    
    std::chrono::duration<double> elapsed = end - start;
    
    std::cout << "Elapsed time: " << elapsed.count() << " s" << endl;  
    
    return 0;
}
